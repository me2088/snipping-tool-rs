
use scrap::{Capturer, Display};

pub fn capture_screenshot() -> Result<(usize, usize, Vec<u32>), Box<dyn std::error::Error>> {
    let display = Display::primary()?;
    let mut capturer = Capturer::new(display)?;
    let (width, height) = (capturer.width(), capturer.height());

    let frame = loop {
        match capturer.frame() {
            Ok(frame) => break frame,
            Err(error) => {
                if error.kind() == std::io::ErrorKind::WouldBlock {
                    // Frame is not ready yet, retry
                    std::thread::sleep(std::time::Duration::from_millis(10));
                } else {
                    return Err(Box::new(error));
                }
            }
        }
    };

    let darkening_factor = 0.4;

    let raw_pixels: Vec<u8> = frame.to_vec();
    let raw_pixels_u32: Vec<u32> = raw_pixels 
        .chunks_exact(4)
        .map(|chunk| {
        (((chunk[3] as f32 * darkening_factor).round() as u32) << 24)
            | (((chunk[2] as f32 * darkening_factor).round() as u32) << 16)
            | (((chunk[1] as f32 * darkening_factor).round() as u32) << 8)
            | ((chunk[0] as f32 * darkening_factor).round() as u32)
    })
        .collect();

    Ok((width as usize, height as usize, raw_pixels_u32))
}