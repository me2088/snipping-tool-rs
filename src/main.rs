mod capture;
mod image_utils;
mod rectangle;

use capture::capture_screenshot;
use image_utils::save_raw_pixels_to_image;
use minifb::{WindowOptions, Window, Key, MouseButton};
use rectangle::draw_rectangle;
use std::{time::{Duration, Instant}, thread};




fn main() {
    let (width, height, mut raw_pixels) = capture_screenshot().expect("Failed to capture screenshot");
    let background_pixels = raw_pixels.clone();
    // save_raw_pixels_to_image(width, height, &raw_pixels, "debug_image.png").expect("Failed to save debug image");

    let mut window = Window::new(
        "",
        width, 
        height,
        WindowOptions {
            scale: minifb::Scale::X1,
            borderless: true,
            title: false,
            topmost: true,
            ..WindowOptions::default()
        },
    )
    .unwrap_or_else(|e| {
        panic!("{}", e)
    });

    let mut start_selection = None;
    let mut end_selection = None;
    let mut is_selecting = false;

    let target_frame_duration = Duration::from_secs_f32(1.0 / 60.0);
    let mut last_frame_time = Instant::now();

    while window.is_open() && !window.is_key_down(Key::Escape) {
        let mouse_pos = window.get_mouse_pos(minifb::MouseMode::Pass).unwrap_or((0.0, 0.0));
        let (mouse_x, mouse_y) = (mouse_pos.0 as usize, mouse_pos.1 as usize);
        
         if window.get_mouse_down(MouseButton::Left) {
            if !is_selecting {
                is_selecting = true;
                start_selection = Some((mouse_x, mouse_y));
            }
            end_selection = Some((mouse_x, mouse_y))
        } else {
            if is_selecting {
                is_selecting = false;
                end_selection = Some((mouse_x, mouse_y));

                // Now you have the starting and ending points of the selection
                println!(
                    "Selection start: {:?}, Selection end: {:?}",
                    start_selection, end_selection
                );
            }
        }

        raw_pixels = background_pixels.clone();

        if let (Some(start), Some(end)) = (start_selection, end_selection) {
            draw_rectangle(&mut raw_pixels, width, start, end, 0x0000FF, 2);
        }
        window.update_with_buffer(&raw_pixels, width, height).unwrap();

        let elapsed_time = Instant::now() - last_frame_time;
            if elapsed_time < target_frame_duration {
            let sleep_duration = target_frame_duration - elapsed_time;
            thread::sleep(sleep_duration);
        }

        last_frame_time = Instant::now();
    }
}
