
use image::{Rgba, ImageBuffer};

pub fn save_raw_pixels_to_image(width: usize, height: usize, raw_pixels: &[u32], path: &str) -> Result<(), image::error::ImageError> {
    let rgba_pixels: Vec<Rgba<u8>> = raw_pixels
        .iter()
        .map(|&pixel| Rgba([
            ((pixel >> 24) & 0xff) as u8,
            ((pixel >> 16) & 0xff) as u8,
            ((pixel >> 8) & 0xff) as u8,
            (pixel & 0xff) as u8,
        ]))
        .collect();

    let img_buffer = ImageBuffer::from_fn(width as u32, height as u32, |x, y| rgba_pixels[(y * width as u32 + x) as usize]);
    img_buffer.save(path)?;

    Ok(())
}