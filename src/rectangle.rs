use std::os::windows::raw;


pub fn draw_rectangle(
    raw_pixels: &mut Vec<u32>,
    width: usize,
    start: (usize, usize),
    end: (usize, usize),
    color: u32,
    brightness_factor: u32,
) {
    let (min_x, max_x) = (start.0.min(end.0), start.0.max(end.0));
    let (min_y, max_y) = (start.1.min(end.1), start.1.max(end.1));

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            let index = y * width + x;

            // Brighten the pixels inside the rectangle
            if x > min_x && x < max_x && y > min_y && y < max_y {
                let pixel = raw_pixels[index];

                let r = ((pixel >> 16) & 0xFF).saturating_mul(brightness_factor);
                let g = ((pixel >> 8) & 0xFF).saturating_mul(brightness_factor);
                let b = (pixel  & 0xFF).saturating_mul(brightness_factor);

                raw_pixels[index] = (r << 16) | (g << 8) | b;
            }

            // Draw the border of the rectangle
            if x == min_x || x == max_x || y == min_y || y == max_y {
                raw_pixels[index] = color;
            }
        }
    }
}